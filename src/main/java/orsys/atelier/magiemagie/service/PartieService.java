package orsys.atelier.magiemagie.service;

import java.util.List;

import orsys.atelier.magiemagie.entitee.Carte;
import orsys.atelier.magiemagie.entitee.Joueur;
import orsys.atelier.magiemagie.entitee.Partie;
import orsys.atelier.magiemagie.entitee.Carte.Ingredient;

public interface PartieService {
	
	public int creerPartie(String pseudo, String nomPartie);
	
	public Partie findPartieById(int id);
	
	public void rejoindrePartie(int idPartie, String pseudo);
	
	public int demarrerPartie(int idPartie);
	
	public Carte piocherCarte();
	
	public Joueur findJoueurById(int id, int idPartie);
	
	public void sortInvisibilite(int idPartie);
	
	public void prendreCarteAuHasard(int idPartie, int idVoleur, int idVictime);
	
	public void sortPhiltreAmour(int idPartie, int idVictime);
	
	public void lancerSortilege();
	
	public void hypnose(int idPartie, int idVictime);
	
	public void enleverCartesUtlilisees(Joueur joueur, Ingredient ingredient1, Ingredient ingredient2);
	
	public Carte prendreCarte(Joueur joueur, int idCarte);
	
	public void sommeilProfond(int idPartie, int idVictime);
	
	public List <Carte> divination(int idPartie);
	
}
