package orsys.atelier.magiemagie.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.swing.text.AbstractDocument.LeafElement;

import orsys.atelier.magiemagie.entitee.Carte;
import orsys.atelier.magiemagie.entitee.Carte.Ingredient;
import orsys.atelier.magiemagie.entitee.Joueur;
import orsys.atelier.magiemagie.entitee.Partie;

public class PartieServiceImpl {

	private List<Partie> parties = new ArrayList<>();

	/**
	 * fonction qui va créer une partie
	 * 
	 * @param pseudo
	 * @param nomPartie
	 * @return int
	 */
	public int creerPartie(String pseudo, String nomPartie) {

		Joueur joueur = new Joueur();
		joueur.setPseudo(pseudo);

		System.out.println(joueur.getPseudo());

		// create party

		Partie partie = new Partie();
		partie.setNom(nomPartie);
		List<Joueur> listeJoueur = partie.getJoueurs();
		listeJoueur.add(joueur);
		partie.setJoueurs(listeJoueur);

		System.out.println(partie.getNom());

		// add party

		parties.add(partie);

		System.out.println("Votre partie a été créée, son id est : " + partie.getId());

		return partie.getId();
	}

	/**
	 * Fonction Lister les parties
	 * 
	 * @return List
	 */
	public List<Partie> listerParties() {
		return this.parties;
	}

	/**
	 * Trouver une partie par son id
	 * 
	 * @param id
	 * @return Partie
	 */
	public Partie findPartieById(int id) {
		Partie thePartie = null;
		for (Partie partie : parties) {
			if (partie.getId() == id) {
				thePartie = partie;
			}
		}
		if (thePartie == null) {
			System.out.println("La partie n'a pas été trouvée !");
		}
		return thePartie;
	}

	/**
	 * Fonction pour rejoindre une partie
	 * 
	 * @param idPartie
	 * @param pseudoJoueur
	 */
	public void rejoindrePartie(int idPartie, String pseudo) {

		Joueur joueur = new Joueur();
		joueur.setPseudo(pseudo);
		// find party
		Partie partie = findPartieById(idPartie);

		// add
		partie.getJoueurs().add(joueur);

		System.out.println("Vous avez rejoint la partie numéro : " + idPartie);
	}

	/**
	 * Fonction pour démarrer la partie
	 */
	public int demarrerPartie(int idPartie) {

		// return si un seul joueur
		Partie partie = findPartieById(idPartie);
		if (partie.getJoueurs().size() <= 1) {
			System.out.println("Vous êtes le seul joueur, la partie ne peut pas commencer !");
			return idPartie;
		}
		
		// distribuer les cartes
		partie.getJoueurs().forEach(joueur -> {
			for (int i = 0; i < 7; i++) {
				List<Carte> cartesDuJoueur = joueur.getCartes();
				cartesDuJoueur.add(piocherCarte());
			}
		});
		
		// donner la main au joueur qui va commencer (prendre celui qui a l'id la plus
		// faible)
		int id = 0;
		Joueur joueurMain = null;

		for (Joueur joueur : partie.getJoueurs()) {
			if (id == 0) {
				joueurMain = joueur;
				id = joueur.getId();
			} else {
				if (id > joueur.getId()) {
					joueurMain = joueur;
				}
			}
		}
		// on donne la main au joueur
		partie.setJoueurQuiALaMain(joueurMain);
		
		return idPartie;
	}

	/**
	 * Fonction pour piocher une carte de manière aléatoire.
	 */
	public Carte piocherCarte() {
		Carte carte = new Carte();
		Random rand = new Random();
		int number = rand.nextInt(4 - 1 + 1) + 1;

		switch (number) {
		case 1:
			carte.setIngredient(Carte.Ingredient.AILE_DE_CHAUVE_SOURIS);
			break;
		case 2:
			carte.setIngredient(Carte.Ingredient.BAVE_DE_CRAPAUD);
			break;
		case 3:
			carte.setIngredient(Carte.Ingredient.MANDRAGORE);
			break;
		case 4:
			carte.setIngredient(Carte.Ingredient.LAPIS_LAZULIS);
			break;
		}

		return carte;
	}
	
	//fonction à utiliser lorsqu'on commence la partie
	
	/**
	 * fonction pour lister les cartes d'un joueur
	 * @param idPartie
	 * @param idJoueur
	 * @return List Carte
	 */
	public List<Carte> listerCarteJoueur(int idPartie, int idJoueur) {
		Partie partie = findPartieById(idPartie);
		Joueur joueur = null;
		
		for(Joueur findJoueur : partie.getJoueurs()) {
			if(findJoueur.getId() == idJoueur) {
				joueur = findJoueur;
			}
		}		
		return joueur.getCartes();		
	}
	
	/**
	 * Function qui passe un tour ou décide de la fin de la partie
	 * On doit vérifier qu 'il reste au moins un joueur sinon partie gagné
	 * On doit vérifier que le joueur n'est pas endormi sinon on enlève -1 ou on continue
	 * On doit vérifier que le joueur possède au moins une carte
	 */
	public void passerJoueurSuivantOuFinDePartie(int idPartie) {
		//cette méthode vérifie que le joueur peut encore jouer
		 Partie partie = findPartieById(idPartie);		
		 Joueur joueurBase = partie.getJoueurQuiALaMain();				
		
		 if(partie.getJoueurs().size() < 2) {
			//la partie est terminée
			 
		 }
	
		//la main passe au joueur suivant
			int indexJoueurMain = partie.getJoueurs().indexOf(joueurBase);		
			int tailleListeJoueur = partie.getJoueurs().size() - 1;
			Joueur newJoueurMain = null;			
				
			if(indexJoueurMain >= tailleListeJoueur){
				newJoueurMain = partie.getJoueurs().get(0);
				partie.setJoueurQuiALaMain(newJoueurMain);
			}
			else {
				indexJoueurMain += 1;
				newJoueurMain = partie.getJoueurs().get(indexJoueurMain);
				partie.setJoueurQuiALaMain(newJoueurMain);
			}				
		
		List<Carte> cartesJoueur = newJoueurMain.getCartes();
		
		
		
	}
	
	/**
	 * Fonction pour passer son tour
	 * le joueur pioche une carte
	 * la main change et passe au joueur suivant
	 */
	public void passerTour(int idPartie, int idJoueur) {
		
		Partie partie = findPartieById(idPartie);		
		
		//piocher une carte
		
		System.out.println(partie.getJoueurQuiALaMain().getPseudo());
		
		Joueur joueur = partie.getJoueurQuiALaMain();		
		List<Carte> cartesJoueur = joueur.getCartes();		
		cartesJoueur.add(piocherCarte());		
		System.out.println(cartesJoueur.toString());					
	}
	
	/**
	 * Trouver un joueur par son id
	 * @param id
	 * @return Joueur
	 */
	public Joueur findJoueurById(int id, int idPartie) {
		Partie partie = findPartieById(idPartie);		
		Joueur joueur = partie.getJoueurs().stream()
				  .filter(playeur -> playeur.getId() == id)
				  .findAny()
				  .orElse(null);		
		return joueur;		
	}
	
	/**
	 * Prendre une carte au hasard dans une liste d'un adversaire
	 * @param idPartie
	 * @param idVoleur
	 * @param idVictime
	 */
	public void prendreCarteAuHasard(int idPartie, int idVoleur, int idVictime) {
		Partie partie = findPartieById(idPartie);		
		Joueur voleur = findJoueurById(idVoleur, partie.getId());
		Joueur victime = findJoueurById(idVictime, partie.getId());
		
		//lister les cartes de la victime et récupérer une carte aléatoire
		
		List<Carte> cartesVictime = victime.getCartes();
		int nombreCartesVictime = cartesVictime.size() - 1;
		
		Random rand = new Random();
		int number = rand.nextInt(nombreCartesVictime - 0 + 0) + 0;
		Carte carte = cartesVictime.get(number);
		
		//ajouter la carte chez le voleur
		
		List<Carte> listeCarteVoleur= voleur.getCartes();
		listeCarteVoleur.add(carte);		
		voleur.setCartes(listeCarteVoleur);		
		
		//enlever la carte chez la victime
		cartesVictime.remove(number);	
	}
	
	/**
	 * Le joueur enlève une carte de sa liste
	 * @param joueur
	 * @param idCarte
	 * @return Carte
	 */
	public Carte prendreCarte(Joueur joueur, int idCarte) {								
		//lister les cartes 		
		List<Carte> cartesJoueur = joueur.getCartes();
		Carte carteRecup = null;		
		for(Carte carte : cartesJoueur) {
			if(carte.getId() == idCarte) {
				carteRecup = carte;
				cartesJoueur.remove(carte);
			}
		}
		return carteRecup;
	}
	
	// ** Liste des sortilèges **
	
	/**
	 * Fonction qui enlève deux cartes à un joueur
	 * @param joueur
	 * @param ingredient1
	 * @param ingredient2
	 */
	public void enleverCartesUtlilisees(Joueur joueur, Ingredient ingredient1, Ingredient ingredient2) {
		for(Carte carte : joueur.getCartes()) {
			if(carte.getIngredient() == ingredient1 || carte.getIngredient() == ingredient2) {				
				 joueur.getCartes().remove(carte);
			}
		}
	}
	
	/**
	 * Le joueur prend une carte chez tous les adversaires
	 * Ingrédients: corne de licorne + bave de crapaud
	 */
	public void sortInvisibilite(int idPartie) {
		Partie partie = findPartieById(idPartie);	
		
		//on doit avoir l'id de la personne qui lance le sortilège
		Joueur joueurMain = partie.getJoueurQuiALaMain();
			
		//enlever les deux cartes au joueurs
		enleverCartesUtlilisees( joueurMain, Ingredient.CORNE_LICORNE, Ingredient.BAVE_DE_CRAPAUD);
				
		//on va devoir boucler pour trouver les joueurs a qui enlever une carte
		
		for(Joueur joueur : partie.getJoueurs()) {
			if(joueur.getId() == joueurMain.getId()) {
				continue;
			}
			else {
				prendreCarteAuHasard(partie.getId(), joueurMain.getId(), joueur.getId());
			}
		}
	}
	
	/**
	 * Le joueur de votre choix vous donne la moitié de ses cartes(au hasard). 
	 * S’il ne possède qu’une carte il a perdu.
	 * Ingrédients: corne de licorne + mandragore
	 * à terminer
	 * @param idPartie
	 * @param idVictime
	 */
	public void sortPhiltreAmour(int idPartie, int idVictime) {
		Partie partie = findPartieById(idPartie);		
		Joueur victime = findJoueurById(idVictime, partie.getId());		
		
		//on doit avoir la personne qui lance le sortilège
		Joueur joueurMain = partie.getJoueurQuiALaMain();
		
		//enlever les deux cartes au joueurs
		enleverCartesUtlilisees( joueurMain, Ingredient.CORNE_LICORNE, Ingredient.MANDRAGORE);
		
		if(victime.getCartes().size() < 2) {
			//le joueur a perdu
		}
		
		 int nbCartesADonner = victime.getCartes().size() / 2;
		 
		 for(int i = 0; i < nbCartesADonner; i++) {
			 prendreCarteAuHasard(partie.getId(), joueurMain.getId(), victime.getId());
		 }
		
	}
	
	/**
	 *  Le joueur échange une carte de son choix contre trois cartes(au hasard) de la victime qu’il choisit
	 *  le joueur va devoir lister ses cartes pour choisir celle à donner
	 * @param idPartie
	 * @param idVictime
	 */
	public void hypnose(int idPartie, int idVictime, int idCarte) {
		
		 Partie partie = findPartieById(idPartie);		
		Joueur victime = findJoueurById(idVictime, partie.getId());		
		
		//on doit avoir la personne qui lance le sortilège
		Joueur joueurMain = partie.getJoueurQuiALaMain();
		
		//enlever les deux cartes au joueurs
		enleverCartesUtlilisees( joueurMain, Ingredient.BAVE_DE_CRAPAUD, Ingredient.LAPIS_LAZULIS);
		
		//la victime perd 3 cartes
		for(int i = 0; i < 3; i++) {
			 prendreCarteAuHasard(partie.getId(), joueurMain.getId(), victime.getId());
		 }
		
		//le joueur donne a la victime une carte choisie
		Carte carte =  prendreCarte( joueurMain, idCarte);
		victime.getCartes().add(carte);
	}
	
	/**
	 * Le joueur peut voir les cartes de tous les autres joueurs
	 * @param idPartie
	 * @return List<Carte>
	 */
	public List <Carte> divination(int idPartie) {
		
		List<Carte> listCartes = new ArrayList<>();
			
		Partie partie = findPartieById(idPartie);		
		Joueur joueurMain = partie.getJoueurQuiALaMain();
		
		//enlever les deux cartes au joueurs
		enleverCartesUtlilisees( joueurMain, Ingredient.AILE_DE_CHAUVE_SOURIS, Ingredient.LAPIS_LAZULIS);
		
		//afficher les cartes des autres joueurs
				
		for(Joueur user : partie.getJoueurs()) {
			if(joueurMain.getId() == user.getId()) {
				continue;
			}		
			else {
				for(Carte carteJoueur : user.getCartes()) {
					listCartes.add(carteJoueur);
				}
			}			
		}
		return listCartes;
	}
	
	/**
	 * La victime  ne peut pas lancer de sorts pendant 2 tours
	 * @param idPartie
	 */
	public void sommeilProfond(int idPartie, int idVictime) {
		
		 Partie partie = findPartieById(idPartie);		
		Joueur joueurMain = partie.getJoueurQuiALaMain();
		
		//enlever les deux cartes au joueurs
		enleverCartesUtlilisees( joueurMain, Ingredient.AILE_DE_CHAUVE_SOURIS, Ingredient.MANDRAGORE);
		
		//la victime ne pourra pas jouer pendant deux tours
		Joueur victime = findJoueurById(idVictime, partie.getId());
		int tourAAttendre = victime.getNombreDeToursAAttendre();
		
		//on détermine le nombre selon le chiffre
		
		if(tourAAttendre >= 2) {
			victime.setNombreDeToursAAttendre(2);		
		}
		else if(tourAAttendre == 1) {
			tourAAttendre++;
			victime.setNombreDeToursAAttendre(tourAAttendre);		
		}
		else {
			victime.setNombreDeToursAAttendre(2);		
		}		
	}
	
	// ** fin liste des sortilèges **
	
	/**
	 * Méthode pour lancer un sortilège.
	 * Elle fera les vérification pour savoir si le joueur peut lancer un sortilège
	 * à terminer
	 */
	public void lancerSortilege(int idPartie) {
		
		 Partie partie = findPartieById(idPartie);
		 Joueur joueurMain = partie.getJoueurQuiALaMain();
		 
		//vérification d'un nombre suffisant de cartes
		if(joueurMain.getCartes().size() < 2) {
			
		}
	}
}




