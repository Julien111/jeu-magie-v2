package orsys.atelier.magiemagie.entite;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import orsys.atelier.magiemagie.entitee.Carte;
import orsys.atelier.magiemagie.entitee.Joueur;
import orsys.atelier.magiemagie.entitee.Carte.Ingredient;

public class JoueurTest {
	
	@Test
	@DisplayName("Tester des getters setters")
    void testerGettersSetters() {
		List<Carte> cartes = new ArrayList<>();		
		Carte carte = null;
		for(int i = 1; i <= 7; i++) {
			carte = new Carte();
			if(i%2 == 0) {
				carte.setIngredient(Ingredient.AILE_DE_CHAUVE_SOURIS);
			}
			else {
				carte.setIngredient(Ingredient.BAVE_DE_CRAPAUD);
			}
			cartes.add(carte);
		}		
		
		Joueur joueur = new Joueur();		
		joueur.setPseudo("joueur1");
		joueur.setNombreDeToursAAttendre(2);
		joueur.setCartes(cartes);
		
		 // On demande à JUnit de vérifier que l'id de la table n'est pas nul
        assertNotNull(joueur.getId());
        assertTrue(joueur.getId() > 0);

        // On vérifie que les getters fonctionnent
        
        assertTrue(joueur.getCartes().size() > 0);
        assertTrue(joueur.getPseudo().equals("joueur1"));
        assertEquals(7, joueur.getCartes().size());
        assertEquals(2, joueur.getNombreDeToursAAttendre());
		
	}
}
