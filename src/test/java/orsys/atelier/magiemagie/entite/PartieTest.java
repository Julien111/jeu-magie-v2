package orsys.atelier.magiemagie.entite;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import orsys.atelier.magiemagie.entitee.Carte;
import orsys.atelier.magiemagie.entitee.Joueur;
import orsys.atelier.magiemagie.entitee.Partie;
import orsys.atelier.magiemagie.entitee.Carte.Ingredient;

public class PartieTest {
	
	@Test
	@DisplayName("Tester des getters setters")
	void testGetterSetter () {
		
		Partie partie1 = new Partie();
		partie1.setNom("partie1");		
		Partie partie2 = new Partie();
		partie2.setNom("partie2");
		
		//création d'une liste de cartes pour faire le test
		
		List<Carte> cartes = new ArrayList<>();		
		
		Carte carte = null;
		
		for(int i = 1; i <= 7; i++) {
			carte = new Carte();
			if(i%2 == 0) {
				carte.setIngredient(Ingredient.AILE_DE_CHAUVE_SOURIS);
			}
			else {
				carte.setIngredient(Ingredient.BAVE_DE_CRAPAUD);
			}
			cartes.add(carte);
		}		
		
		//création liste de joueurs
		
		Joueur joueur = null;
		
		List<Joueur> joueurs = new ArrayList<>();
		for(int i = 0; i < 4; i++) {
			joueur = new Joueur();
			joueur.setPseudo("joueur"+ i);
			joueur.setCartes(cartes);
			joueur.setNombreDeToursAAttendre(0);
			joueurs.add(joueur);
		}	
		partie1.setJoueurs(joueurs);
		partie2.setJoueurs(joueurs);		
		
		//joueur qui a la main
		Joueur joueurOne = joueurs.get(0);
		partie1.setJoueurQuiALaMain(joueurOne);
		partie2.setJoueurQuiALaMain(joueurOne);
		
		 // On demande à JUnit de vérifier que l'id de la table n'est pas nul
        assertNotNull(partie1.getId());
        assertNotNull(partie2.getId());
        assertTrue(partie1.getId() > 0);       
        assertTrue(partie2.getId() > 0);
        
        //objet partie
        assertTrue(partie1.getNom().equals("partie1"));
        assertTrue(partie2.getNom().equals("partie2"));
        
        assertEquals(4, partie2.getJoueurs().size());
        assertEquals(4, partie1.getJoueurs().size());        
        assertEquals(7, joueurOne.getCartes().size());        
        assertEquals(1, partie1.getJoueurQuiALaMain().getId());              
		
	}
}
