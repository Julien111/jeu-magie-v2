package orsys.atelier.magiemagie.entite;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import orsys.atelier.magiemagie.entitee.Carte;
import orsys.atelier.magiemagie.entitee.Carte.Ingredient;

public class CarteTest {
	
	@Test
	@DisplayName("Tester des getters setters")
	void testGetterSetter () {
		
				
		Carte carte1 = new Carte();
		carte1.setIngredient(Ingredient.AILE_DE_CHAUVE_SOURIS);
		
		Carte carte2 = new Carte();
		carte2.setIngredient(Ingredient.MANDRAGORE);
		
		Carte carte5 = new Carte();
		carte5.setIngredient(Ingredient.LAPIS_LAZULIS);
		
		Carte carte3 = new Carte();
		carte3.setIngredient(Ingredient.CORNE_LICORNE);
		
		Carte carte4 = new Carte();
		carte4.setIngredient(Ingredient.BAVE_DE_CRAPAUD);
		
		// On demande à JUnit de vérifier que l'id de la table n'est pas nul
        assertNotNull(carte1.getId());
        assertTrue(carte1.getId() > 0);
        
        //test des ingrédients		
		
		assertEquals(Ingredient.AILE_DE_CHAUVE_SOURIS, carte1.getIngredient());
		assertEquals(Ingredient.MANDRAGORE, carte2.getIngredient());
		assertEquals(Ingredient.CORNE_LICORNE, carte3.getIngredient());
		assertEquals(Ingredient.BAVE_DE_CRAPAUD, carte4.getIngredient());
		assertNotEquals(Ingredient.BAVE_DE_CRAPAUD, carte2.getIngredient());
		assertNotNull(carte4);		
		assertNotNull(carte4.getIngredient());		
		
	}	
	
}
